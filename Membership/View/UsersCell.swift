//
//  UsersCell.swift
//  Membership
//
//  Created by Administrator on 1/16/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class UsersCell: UITableViewCell {
    
    @IBOutlet weak var LabelName : UILabel!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var LabelAge : UILabel!
    @IBOutlet weak var textViewDescription : UITextView!
    //MARK: - Set data with User Model
    var item : User?  {
        didSet{
            
        
        }
    }
    //MARK: - Set data with Person Model
    var Item : Person?{
        didSet{
            
            ProfileImage.image = UIImage(data: Item?.image! as! Data, scale: 0.3)
            LabelName.text = Item?.name
        
            LabelAge.text = String(describing: Item!.age)
            textViewDescription.text = Item?.decription
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
