//
//  AddViewController.swift
//  Membership
//
//  Created by Administrator on 1/16/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import CoreData

class AddViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var ProfileImageVIew: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFielAge: UITextField!
    @IBOutlet weak var textViewDiscription: UITextView!
    var imagePicker = UIImagePickerController()
    var arrayAge = [Int]()
    var person :Person?
    var isNew = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
       
        
        let AgePickerView = UIPickerView()
        AgePickerView.delegate = self
        textFielAge.inputView = AgePickerView
        self.imagePicker.delegate = self
        
        // Do any additional setup after loading the view.
        self.setup()
        self.setAgeData()
    }
    override func viewWillAppear(_ animated: Bool) {
        //MARK: - Show infomation sellected row
        if let person = person{
            textFieldName.text = person.name
            textFielAge.text = String(person.age)
            textViewDiscription.text = person.decription
            ProfileImageVIew.image = UIImage(data: (person.image as! NSData) as Data, scale: 0.3)
        }
    }

    
    //MARK : - Get age
    func setAgeData(){
        for i in 1...100{
            arrayAge.append(i)
        }
    }
    //MARK: - Add TapGestureRecognizier
    func setup() {
        ProfileImageVIew.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(loadImage(sender:)))
        ProfileImageVIew.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   //MARK: - Load image from Library
    @objc func loadImage(sender : UITapGestureRecognizer) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    //MARK: - Set ImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            ProfileImageVIew.image = pickerImage
        }else if let pickerImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            ProfileImageVIew.image = pickerImage
        }
        self.dismiss(animated: true, completion: nil)
        ProfileImageVIew.contentMode = .scaleAspectFit
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - PickerView stack
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Int(arrayAge.count)
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(arrayAge[row])
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textFielAge.text = String(arrayAge[row])
    }

    
    //MARK : - Taped button save
    @IBAction func ButtonSave(_ sender: Any) {
        //MARK : - Add Member
        if isNew {
            let person = Person(context: PersistenceServce.context)
            person.age = Int16(textFielAge.text!)!
            person.name = textFieldName.text
            person.decription = textViewDiscription.text
            let dataImage = UIImagePNGRepresentation(ProfileImageVIew.image!)
            person.image = dataImage! as NSData
            
            
            PersistenceServce.saveContext()
            //MARK : - Edit and Update Member
        }else{
            
            let fetchRequest : NSFetchRequest<Person> = Person.fetchRequest()
            do{
                let results = try PersistenceServce.context.fetch(fetchRequest)
                for i in 0...results.count-1{
                    if results[i] == person{
                        
                        results[i].age = Int16(textFielAge.text!)!
                        results[i].name = textFieldName.text
                        results[i].decription = textViewDiscription.text
                        let dataImage = UIImagePNGRepresentation(ProfileImageVIew.image!)
                        results[i].image = dataImage! as NSData
                        PersistenceServce.saveContext()
                    }
                }
                
            } catch{
                
            }
        }
        
        navigationController?.popViewController(animated: true)
        
        
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
