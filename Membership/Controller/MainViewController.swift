//
//  MainViewController.swift
//  Membership
//
//  Created by Administrator on 1/16/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UITableViewController {
    
    var users : [User]?
    var people = [Person]()
 
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.rowHeight = 150
        tableView.alwaysBounceVertical = true
        fetchRequest()
    }
     //Mark : - Request Data
    func fetchRequest(){
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let people = try PersistenceServce.context.fetch(fetchRequest)
          
            self.people = people
            self.tableView.reloadData()
            
            
        } catch {}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK : - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return people.count
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UsersCell
        cell.Item = people[indexPath.row]
        
        return cell
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //MARK : - Delete row
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            people.remove(at: indexPath.row)
            let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()

            do {
                let results = try PersistenceServce.context.fetch(fetchRequest)
                PersistenceServce.context.delete(results[indexPath.row])
                PersistenceServce.saveContext()


            } catch {}

          }
            self.tableView.reloadData()

        }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"
        {
            let vc = segue.destination as? AddViewController
            let indexpath = tableView.indexPathForSelectedRow
            let selected = people[(indexpath?.row)!]
            vc?.person = selected
            vc?.isNew = false
            
            
           
        }
    }
    //MARK: - Logout App
    @IBAction func ButtonLogout(_ sender:Any){
       
        let exit = UIAlertController(title: "Confirm", message: "Are you sure you want to log out", preferredStyle: UIAlertControllerStyle.alert)
        exit.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action:UIAlertAction) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        exit.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(exit, animated: true, completion: nil)
        

    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
