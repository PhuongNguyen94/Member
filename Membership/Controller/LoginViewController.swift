//
//  ViewController.swift
//  Membership
//
//  Created by Administrator on 1/15/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var txt_ID: UITextField!
    @IBOutlet weak var txt_pass: UITextField!
    
    let users = ["phuong":"123","tu":"123"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK : - Set Style ****
        txt_pass.isSecureTextEntry = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        txt_ID.text = ""
        txt_pass.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func bnt_login(_ sender: Any) {
        //MARK: - Set logic Login
        if let pass = users[txt_ID.text!]{
            if pass == txt_pass.text {
                UserDefaults.standard.setValue(users, forKey: "Users")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "view2") as! MainViewController
                navigationController?.pushViewController(vc, animated: true)
            }else{
                let alert = UIAlertController(title: "Sai Mat Khau", message: "Your credentials are incorrect, please try again.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
                
                self.present(alert, animated: true)
            }
        }else{
            let alert = UIAlertController(title: "User not fount", message: "User doesn't exist in our system, please try again..", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            
            
            self.present(alert, animated: true)
        }

    }
    

}

