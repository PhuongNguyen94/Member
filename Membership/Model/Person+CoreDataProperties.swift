//
//  Person+CoreDataProperties.swift
//  Membership
//
//  Created by Administrator on 1/17/18.
//  Copyright © 2018 Administrator. All rights reserved.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var image: NSData?
    @NSManaged public var decription: String?

}
